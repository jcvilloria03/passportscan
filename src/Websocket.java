import java.io.FileInputStream;
import java.io.IOException;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.URISyntaxException;
import java.net.UnknownHostException;
import java.util.Properties;
import java.util.concurrent.CompletableFuture;

import org.json.JSONException;
import org.json.JSONObject;

import io.socket.client.IO;
import io.socket.client.Socket;
import io.socket.emitter.Emitter.Listener;

public class Websocket {	
	//private String SOCKET_SERVER = "http://47.74.227.176:8010";
		//private String SOCKET_SERVER = "http://18.138.54.66:8010";
		private String SOCKET_SERVER = "https://demo.palvision.com:8009";
		//private String SOCKET_SERVER = "https://18.136.245.107:8010";
		//private String SOCKET_SERVER = "http://172.17.0.10:8010";
		//private String SOCKET_SERVER = "http://192.168.1.224:8010";
		private String SOCKET_NAMESPACE = "";
		private String SOCKET_NAMESPACE2 = "/device";
		
		private Socket socket;
		private Socket socket2;
		
		private String[] arrResult;
		private String[] arrName;
		
		private boolean hasData = false;
		private boolean hasError = false;
		
		public Websocket() throws IOException {
	        try {
	        	
	        	InetAddress ip;
	        	        	
	        	Properties props_con = new Properties();
	        	props_con.load(new FileInputStream("demo.properties"));
	    	
	    		String _mac_address = props_con.getProperty("mac_address");
	    		System.out.println(_mac_address);
	    		
	    		try {
	    			
	    			ip = InetAddress.getLocalHost();
	    			System.out.println("Current IP address : " + ip.getHostAddress());
	    			
	    			NetworkInterface network = NetworkInterface.getByInetAddress(ip);
	    				
	    			byte[] mac = network.getHardwareAddress();
	    				
	    			System.out.print("Current MAC address : ");
	    				
	    			StringBuilder sb = new StringBuilder();
	    			for (int i = 0; i < mac.length; i++) {
	    				sb.append(String.format("%02X%s", mac[i], (i < mac.length - 1) ? ":" : ""));		
	    			}
	    			System.out.println(sb.toString());
	    			_mac_address = sb.toString();
	    				
	    		} catch (UnknownHostException e) {
	    			
	    			e.printStackTrace();
	    			
	    		} catch (SocketException e){
	    				
	    			e.printStackTrace();
	    				
	    		}

	    		//_mac_address = "1C:4D:70:36:06:D1";
	    		//_mac_address = "88:B1:11:C0:0E:27";
	    		//_mac_address = "00:0E:C6:E6:47:75";
	    		//_mac_address = "00:0E:C6:E6:47:40";
	    		//_mac_address = "B4:D5:BD:EB:FE:7E";
	    		//UPBoard
	    		//_mac_address = "F0:6E:0B:D1:86:83";

	    		System.out.println(_mac_address);
	        	System.out.println("connect");
	        	String[] transports = {"websocket"};
	    		IO.Options options = new IO.Options();
	            options.transports = transports;
	            options.upgrade = false;
	            options.query = "isJavaApp=true&macAddress="+_mac_address;
	            
	            IO.Options options2 = new IO.Options();
	            options2.transports = transports;
	            options2.upgrade = false;
	            options2.query = "room=LOBBY&macAddress="+_mac_address;
	            
				socket = IO.socket(SOCKET_SERVER+SOCKET_NAMESPACE, options);
				socket.connect();
		        socket.on("connected", onConnection);
		        socket.on("scan", onScan);
		        
		        socket2 = IO.socket(SOCKET_SERVER+SOCKET_NAMESPACE2, options2);
		        socket2.connect();
		        socket2.on("connected", onConnection);
		        socket2.on("rebootPOS", onReboot);
		        
		        //scanPassport("5317480525913680254", _mac_address);
			} catch (URISyntaxException e) {
				e.printStackTrace();
			}
		}
		
		private Listener onConnection = new Listener() {
			@Override
			public void call(Object... args) {
				System.out.println(args[0].toString());
			}
		};
		
		private Listener onScan = new Listener() {
			@Override
			public void call(Object... args) {
				for(int i = 0; i < args.length; i++) {
					try {
						JSONObject data = new JSONObject(args[i].toString());
						socket.emit("scanning", data.getString("macAddress"));
						//scanPassport(data.getString("idCard"), data.getString("confirmationNumber"), data.getString("macAddress"));
						scanPassport(data.getString("idCard"), data.getString("macAddress"));
						
					} catch (JSONException | IOException e) {
						e.printStackTrace();
					}
				}
			}
		};
		
		//private void scanPassport(String idCard, String confirmationNumber, String macAddress) throws IOException {
		private void scanPassport(String idCard, String macAddress) throws IOException {
			//System.out.println("Scanning Reference Number: " + confirmationNumber + " on Scanner ID: " + idCard);	
			System.out.println("Scanning Scanner ID: " + idCard);
			CompletableFuture.supplyAsync(() -> {
				try {
					//return (String)demodll.scan(idCard, confirmationNumber);
					return (String)demodll.scan(idCard);
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
					return null;
				}
			}).thenAccept(i -> {
				// Sending Data back frontend
				JSONObject data = new JSONObject();
				try {
					//System.out.println("data fetch end: " + i);
					//System.out.println(i.contains(":")); 
					
					//System.out.println(i.contains("Error"));
					
					hasError = i.contains("Error");
					if(hasError == true) {
						data.put("error", i);
						socket.emit("finish", data);
					} else {
						System.out.println("abc");
						hasData = i.contains(":");
						System.out.println("hasData ==" + hasData);
						if(hasData == true) {
							System.out.println("efg");
							arrResult = i.split(":");
							
							int _arrcnt = arrResult.length;
							System.out.println(arrResult[_arrcnt-1]); 
							//System.exit(0);
							
							if(arrResult[0].contains("  ")) {
								System.out.println("double"); 
								arrName = arrResult[0].split("  ");
								data.put("firstname", arrName[1]);
								data.put("lastname", arrName[0]);
							} else {
								System.out.println("single");
								arrName = arrResult[0].split(" ");
								int _arrnamecnt = arrName.length;
								System.out.println("_arrnamecnt == " + _arrnamecnt);
								System.out.println("_arrnamecnt-1 == " + (_arrnamecnt-1));
								String strFirst = "";
								String strLast = "";
								System.out.println("b4 for");
								for(int z = 0; z < (_arrnamecnt-1); z++) {
									if(z == 0) {
										strFirst = arrName[z];
									} else {
										strFirst = strFirst + " " + arrName[z];
									}
									System.out.println("strFirst = " + strFirst);
									System.out.println("z = " + z);
								}
								System.out.println("after for");
								strLast = arrName[(_arrnamecnt-1)];
								
								System.out.println(strFirst);
								System.out.println(strLast);
								//data.put("firstname", arrName[0]);
								//data.put("lastname", arrName[1]);
								data.put("firstname", strFirst);
								data.put("lastname", strLast);
							}
							System.out.println("selection");
							if(arrResult[_arrcnt-1].equals("13")) {
								data.put("country", arrResult[1]);
								data.put("birthday", arrResult[2]);
								data.put("gender", arrResult[3]);
								data.put("passportNumber", arrResult[4]);
								data.put("passportExpiration", arrResult[5]);
								data.put("issuingcountry", arrResult[6]);
								data.put("image", arrResult[7]);
								data.put("photo", arrResult[8]);
								data.put("status", arrResult[9]);
								data.put("idtype", arrResult[10]);
							} else if(arrResult[_arrcnt-1].equals("2004")) {
								data.put("country", arrResult[1]);
								data.put("birthday", arrResult[2]);
								data.put("gender", arrResult[3]);
								data.put("passportNumber", arrResult[4]);
								data.put("race", arrResult[5]);
								data.put("image", arrResult[6]);
								data.put("photo", arrResult[7]);
								data.put("status", arrResult[8]);
								data.put("idtype", arrResult[9]);
							} else if(arrResult[_arrcnt-1].equals("2023")) {
								data.put("passportNumber", arrResult[1]);
								data.put("employer", arrResult[2]);
								data.put("image", arrResult[3]);
								data.put("photo", arrResult[4]);
								data.put("status", arrResult[5]);
								data.put("idtype", arrResult[6]);
							}
							//System.out.println("b4 emit finish");
							//socket.emit("finish", data);
							//System.out.println("after emit finish");
						} else {
							System.out.println("data sent: " + i);
							socket.emit("finish", i);
						}
						//if(hasData == true) {
					//		System.out.println("efg");
						//	System.out.println("data sent1: " + data);
						
							//socket.emit("finish", data);
						//}
					}
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				if(hasData == true) {
					
					System.out.println("data sent2: " + data);
					System.out.println("b4 emit finish");
					socket.emit("finish", data);
					System.out.println("after emit finish");
				}
			}).thenRun(() -> System.out.println("finish" ));
			
		}
		
		private Listener onReboot = new Listener() {
			@Override
			public void call(Object... args) {
				Runtime rt=Runtime.getRuntime();
				System.out.println("Reboot");
				try
				{
					Process pr1=rt.exec("cmd /c shutdown -r"); // for restart
				}
				catch(Exception e)
				{
					System.out.println(e.getMessage());	
				}
			}
		};
}
